# ansys2meshio

This package intends to support the zone specification when converting a mesh file from ANSYS format using [meshio](https://pypi.org/project/meshio/). Currently working only with triangle/tetrahedra and quad/hexahedra meshes.

## Dependencies & Installation

- [Python 3](https://www.python.org/downloads/) (3.10.6);
- [meshio](https://pypi.org/project/meshio/) (5.3.4).

You may install ansys2meshio via pip.

```bash
$ pip install git+https://gitlab.gbar.dtu.dk/casfe/ansys2meshio
```

You  may also clone this repository and install it.

```bash
$ git clone https://gitlab.gbar.dtu.dk/casfe/ansys2meshio
$ cd ansys2meshio
$ pip install .
```

## Usage

Read the ANSYS mesh. It returns a [Mesh object](https://github.com/nschloe/meshio/blob/main/src/meshio/_mesh.py) that can be converted to any format supported by meshio.

```py
>>> import ansys2meshio
>>> foo_mesh = ansys2meshio.read('foo.msh')
>>> foo_mesh.write('bar.xdmf')
```

It is also possible to return a dictionary with the zone information.

```py
>>> foo_mesh, zones = ansys2meshio.read('foo.msh', return_zones=True)
>>> print(zones)
{13: ('BODY', 'fluid'), 14: ('int_BODY', 'interior'), 15: ('SURFACE', 'wall'), 16: ('WALL', 'wall')}
```

For writing a mesh in ANSYS format, the dictionary with the zones should be provided, in case the mesh contains walls, otherwise inner facets are considered interior facets. Furthermore, make sure that the written mesh is an ansys2meshio object, not a meshio object.

```py
>>> foo_mesh, zones = ansys2meshio.read('foo.msh', return_zones=True)
>>> bar_mesh = ansys2meshio.Mesh(points=foo_mesh.points, cells=foo_mesh.cells, cell_data=foo_mesh.cell_data)
>>> bar_mesh.write('bar.msh', zones=zones)
```