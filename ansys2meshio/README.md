# ansys2meshio

This package intends to support the zone specification when converting a mesh file from ANSYS format using [meshio](https://pypi.org/project/meshio/). Currently working only with triangle/tetrahedra and quad/hexahedra meshes.

## Dependencies & Installation

- [Python 3](https://www.python.org/downloads/) (3.10.6);
- [meshio](https://pypi.org/project/meshio/) (5.3.4).