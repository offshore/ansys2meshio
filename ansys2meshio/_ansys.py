"""
I/O for Ansys's msh format.

<https://romeo.univ-reims.fr/documents/fluent/tgrid/ug/appb.pdf>
"""
import re
from meshio import CellBlock

import numpy as np
from scipy.spatial import Delaunay

from meshio.__about__ import __version__
from meshio._common import warn
from meshio._exceptions import ReadError, WriteError
from meshio._files import open_file
from meshio._helpers import register_format
from meshio._mesh import Mesh


def _skip_to(f, char):
	c = None
	while c != char:
		c = f.read(1).decode()


def _skip_close(f, num_open_brackets):
	while num_open_brackets > 0:
		char = f.read(1).decode()
		if char == "(":
			num_open_brackets += 1
		elif char == ")":
			num_open_brackets -= 1


def _read_points(f, line, first_point_index_overall, last_point_index):
	# If the line is self-contained, it is merely a declaration
	# of the total number of points.
	if line.count("(") == line.count(")"):
		return None, None, None

	# (3010 (zone-id first-index last-index type ND)
	out = re.match("\\s*\\(\\s*(|20|30)10\\s*\\(([^\\)]*)\\).*", line)
	assert out is not None
	a = [int(num, 16) for num in out.group(2).split()]

	if len(a) <= 4:
		raise ReadError()

	first_point_index = a[1]
	# store the very first point index
	if first_point_index_overall is None:
		first_point_index_overall = first_point_index
	# make sure that point arrays are subsequent
	if last_point_index is not None:
		if last_point_index + 1 != first_point_index:
			raise ReadError()
	last_point_index = a[2]
	num_points = last_point_index - first_point_index + 1
	dim = a[4]

	# Skip ahead to the byte that opens the data block (might
	# be the current line already).
	last_char = line.strip()[-1]
	while last_char != "(":
		last_char = f.read(1).decode()

	if out.group(1) == "":
		# ASCII data
		pts = np.empty((num_points, dim))
		for k in range(num_points):
			# skip ahead to the first line with data
			line = ""
			while line.strip() == "":
				line = f.readline().decode()
			dat = line.split()
			if len(dat) != dim:
				raise ReadError()
			for d in range(dim):
				pts[k][d] = float(dat[d])
	else:
		# binary data
		if out.group(1) == "20":
			dtype = np.float32
		else:
			if out.group(1) != "30":
				ReadError(f"Expected keys '20' or '30', got {out.group(1)}.")
			dtype = np.float64
		# read point data
		pts = np.fromfile(f, count=dim * num_points, dtype=dtype).reshape(
			(num_points, dim)
		)

	# make sure that the data set is properly closed
	_skip_close(f, 2)
	return pts, first_point_index_overall, last_point_index


def _read_cells(f, line):
	# If the line is self-contained, it is merely a declaration of the total number of
	# points.
	if line.count("(") == line.count(")"):
		return None, None, None

	out = re.match("\\s*\\(\\s*(|20|30)12\\s*\\(([^\\)]+)\\).*", line)
	assert out is not None
	a = [int(num, 16) for num in out.group(2).split()]
	if len(a) <= 4:
		raise ReadError()
	cell_id = a[0]
	first_index = a[1]
	last_index = a[2]
	num_cells = last_index - first_index + 1
	zone_type = a[3]
	element_type = a[4]

	if zone_type == 0:
		# dead zone
		return None, None, None

	key, num_nodes_per_cell = {
		0: ("mixed", None),
		1: ("triangle", 3),
		2: ("tetra", 4),
		3: ("quad", 4),
		4: ("hexahedron", 8),
		5: ("pyramid", 5),
		6: ("wedge", 6),
	}[element_type]

	# Skip to the opening `(` and make sure that there's no non-whitespace character
	# between the last closing bracket and the `(`.
	if line.strip()[-1] != "(":
		c = None
		while True:
			c = f.read(1).decode()
			if c == "(":
				break
			if not re.match("\\s", c):
				# Found a non-whitespace character before `(`.
				# Assume this is just a declaration line then and
				# skip to the closing bracket.
				_skip_to(f, ")")
				return None, None, None

	if key == "mixed":
		# From
		# <https://www.afs.enea.it/project/neptunius/docs/fluent/html/ug/node1470.htm>:
		#
		# > If a zone is of mixed type (element-type=0), it will have a body that
		# > lists the element type of each cell.
		#
		# No idea where the information other than the element types is stored
		# though. Skip for now.
		data = None
	else:
		# read cell data
		if out.group(1) == "":
			# ASCII cells
			data = np.empty((num_cells, num_nodes_per_cell), dtype=int)
			for k in range(num_cells):
				line = f.readline().decode()
				dat = line.split()
				if len(dat) != num_nodes_per_cell:
					raise ReadError()
				data[k] = [int(d, 16) for d in dat]
		else:
			if key == "mixed":
				raise ReadError("Cannot read mixed cells in binary mode yet")
			# binary cells
			if out.group(1) == "20":
				dtype = np.int32
			else:
				if out.group(1) != "30":
					ReadError(f"Expected keys '20' or '30', got {out.group(1)}.")
				dtype = np.int64
			shape = (num_cells, num_nodes_per_cell)
			count = shape[0] * shape[1]
			data = np.fromfile(f, count=count, dtype=dtype).reshape(shape)

	# make sure that the data set is properly closed
	_skip_close(f, 2)
	return key, data, cell_id


def _read_faces(f, line):
	# faces
	# (13 (zone-id first-index last-index type element-type))

	# If the line is self-contained, it is merely a declaration of
	# the total number of points.
	if line.count("(") == line.count(")"):
		return {}, None, [], None, None

	out = re.match("\\s*\\(\\s*(|20|30)13\\s*\\(([^\\)]+)\\).*", line)
	assert out is not None
	a = [int(num, 16) for num in out.group(2).split()]

	if len(a) <= 4:
		raise ReadError()
	facet_id = a[0]
	first_index = a[1]
	last_index = a[2]
	num_cells = last_index - first_index + 1
	element_type = a[4]

	element_type_to_key_num_nodes = {
		0: ("mixed", None),
		2: ("line", 2),
		3: ("triangle", 3),
		4: ("quad", 4),
	}

	key, num_nodes_per_cell = element_type_to_key_num_nodes[element_type]

	# Skip ahead to the line that opens the data block (might be
	# the current line already).
	if line.strip()[-1] != "(":
		_skip_to(f, "(")

	data = {}
	if out.group(1) == "":
		# ASCII
		if key == "mixed":
			# From
			# <https://www.afs.enea.it/project/neptunius/docs/fluent/html/ug/node1471.htm>:
			#
			# > If the face zone is of mixed type (element-type = > 0), the body of the
			# > section will include the face type and will appear as follows
			# >
			# > type v0 v1 v2 c0 c1
			# >
			for k in range(num_cells):
				line = ""
				while line.strip() == "":
					line = f.readline().decode()
				dat = line.split()
				type_index = int(dat[0], 16)
				if type_index == 0:
					raise ReadError()
				type_string, num_nodes_per_cell = element_type_to_key_num_nodes[
					type_index
				]
				if len(dat) != num_nodes_per_cell + 3:
					raise ReadError()

				if type_string not in data:
					data[type_string] = []

				data[type_string].append(
					[int(d, 16) for d in dat[1 : num_nodes_per_cell + 1]]
				)

			data = {key: np.array(data[key]) for key in data}

		else:
			# read cell data
			data = np.empty((num_cells, num_nodes_per_cell), dtype=int)
			bounds = np.empty((num_cells, 2), dtype=int)
			for k in range(num_cells):
				line = f.readline().decode()
				dat = line.split()
				# The body of a regular face section contains the grid connectivity, and
				# each line appears as follows:
				#   n0 n1 n2 cr cl
				# where n* are the defining nodes (vertices) of the face, and c* are the
				# adjacent cells.
				if len(dat) != num_nodes_per_cell + 2:
					raise ReadError()
				data[k] = [int(d, 16) for d in dat[:num_nodes_per_cell]]
				bounds[k] = [int(d, 16) for d in dat[num_nodes_per_cell:]]
			data = {key: data}
	else:
		# binary
		if out.group(1) == "20":
			dtype = np.int32
		else:
			if out.group(1) != "30":
				ReadError(f"Expected keys '20' or '30', got {out.group(1)}.")
			dtype = np.int64

		if key == "mixed":
			raise ReadError("Mixed element type for binary faces not supported yet")

		# Read cell data.
		# The body of a regular face section contains the grid
		# connectivity, and each line appears as follows:
		#   n0 n1 n2 cr cl
		# where n* are the defining nodes (vertices) of the face,
		# and c* are the adjacent cells.
		shape = (num_cells, num_nodes_per_cell + 2)
		count = shape[0] * shape[1]
		data = np.fromfile(f, count=count, dtype=dtype).reshape(shape)
		# Cut off the adjacent cell data.
		data = data[:, :num_nodes_per_cell]
		data = {key: data}

	# make sure that the data set is properly closed
	_skip_close(f, 2)

	return data, facet_id, bounds, first_index, last_index


def _read_zones(f, line):
	# faces
	# (39 (zone-id type name)())
	out = re.match("\\s*\\(\\s*(|20|30)39\\s*\\(([^\\)]+)\\).*", line)
	a = out.group(2).split()

	if len(a) != 3:
		raise ReadError()

	return a[1], a[2], int(a[0])


def _get_cell_info(f, line):
	out = re.match("\\s*\\(\\s*(|20|30)12\\s*\\(([^\\)]+)\\).*", line)
	a = [int(num, 16) for num in out.group(2).split()]

	if len(a) <= 4:
		raise ReadError()

	cell_id = a[0]
	first_index = a[1]
	last_index = a[2]
	num_cells = last_index - first_index + 1
	zone_type = a[3]
	element_type = a[4]

	key, num_nodes_per_cell = {
		0: ("mixed", None),
		1: ("triangle", 3),
		2: ("tetra", 4),
		3: ("quad", 4),
		4: ("hexahedron", 8),
		5: ("pyra", 5),
		6: ("wedge", 6),
	}[element_type]

	return key, num_nodes_per_cell, num_cells, cell_id, first_index, last_index


def _add_facet_nodes_to_cell(facet, cells, cell_1, cell_2):
	for thread in range(0, len(cells)):
		first_index = cells[thread][2]
		last_index = cells[thread][3]

		if cell_1 >= first_index and cell_1 <= last_index:
			cell_nodes = cells[thread][1][cell_1 - first_index]
			cell_nodes = _join_nodes(cell_nodes, facet)
			cells[thread][1][cell_1 - first_index] = cell_nodes

		if cell_2 >= first_index and cell_2 <= last_index:
			cell_nodes = cells[thread][1][cell_2 - first_index]
			cell_nodes = _join_nodes(cell_nodes, facet)
			cells[thread][1][cell_2 - first_index] = cell_nodes

	return cells


def _join_nodes(cell_nodes, facet):
	nodes = np.trim_zeros(cell_nodes)
	if len(cell_nodes) == 4: # tetra
		nodes = [*nodes, *facet[~np.isin(facet, nodes, assume_unique=True)]]
	elif len(cell_nodes) == 8: # hexa
		if all(~np.isin(facet, cell_nodes, assume_unique=True)):
			nodes = np.array([*nodes, *facet])
	num_nodes = len(nodes)
	cell_nodes[:num_nodes] = nodes
	return cell_nodes


def _assign_id_to_element(element_data, elements, element_id, key):
	num_of_elements = 0

	for thread in range(len(element_id)):
		for _ in range(len(elements[thread][1])):
			num_of_elements += 1
	
	element_values = np.array([[0]] * num_of_elements, dtype=np.uint64)
	for thread in range(len(element_id)):
		first_index = elements[thread][2]
		last_index = elements[thread][3]
		for cell in range(first_index-1, last_index):
			element_values[cell] += element_id[thread]
	element_data[key] = element_values

	return element_data


def _elements_adjust(elements, tags=None):
	elements_raw = elements
	key = elements_raw[0][0]
	elements = []
	for thread in range(0, len(elements_raw)):
		for element in elements_raw[thread][1]:
			elements.append(element)
	num_elements = len(elements)
	num_nodes_per_element = len(elements[0])
	shape = (num_elements, num_nodes_per_element)
	elements = np.reshape(elements, shape)
	return CellBlock(cell_type=key, data=elements, tags=tags)


def _fix_hexa_orientation(points, hexa):
	facet1 = hexa[:4]
	reference_point = points[facet1][0]
	facet2 = hexa[4:]
	barycenter = np.mean(points[hexa], axis=0)
	facet1_center = np.mean(points[facet1], axis=0)
	e1 = points[facet1][1] - points[facet1][0]
	e2 = points[facet1][2] - points[facet1][0]
	normal1 = np.cross(e1, e2)
	direction1 = barycenter - facet1_center
	facet2_center = np.mean(points[facet2], axis=0)
	e1 = points[facet2][1] - points[facet2][0]
	e2 = points[facet2][2] - points[facet2][0]
	normal2 = np.cross(e1, e2)
	direction2 = barycenter - facet2_center
	if np.dot(normal1, direction1)*np.dot(normal2, direction2) > 0:
		facet2 = facet2[::-1]
	distances = np.linalg.norm(points[facet2] - reference_point, axis=1)
	closest_node_index = np.argmin(distances)
	hexa[4:] = np.roll(facet2, -closest_node_index, axis=0)
	return hexa

def read(filename, return_zones=False, tags='meshtags'):  # noqa: C901
	# Initialize the data optional data fields
	cell_data = {}
	facet_data = {}

	points = []
	cells = []
	facets = []
	bounds = []

	cell_id = []
	facet_id = []
	zones = {}

	first_point_index_overall = None
	last_point_index = None

	# read file in binary mode since some data might be binary
	with open_file(filename, "rb") as f:
		while True:
			line = f.readline().decode()
			if not line:
				break

			if line.strip() == "":
				continue

			# expect the line to have the form
			#  (<index> [...]
			out = re.match("\\s*\\(\\s*([0-9]+).*", line)
			if not out:
				raise ReadError()
			index = out.group(1)

			if index == "0":
				# Comment.
				_skip_close(f, line.count("(") - line.count(")"))
			elif index == "1":
				# header
				# (1 "<text>")
				_skip_close(f, line.count("(") - line.count(")"))
			elif index == "2":
				# dimensionality
				# (2 3)
				_skip_close(f, line.count("(") - line.count(")"))
			elif re.match("(|20|30)10", index):
				# points
				pts, first_point_index_overall, last_point_index = _read_points(
					f, line, first_point_index_overall, last_point_index
				)

				if pts is not None:
					points.append(pts)

			elif re.match("(|20|30)12", index):
				# cells
				# (2012 (zone-id first-index last-index type element-type))
				key, data, global_id = _read_cells(f, line)
				if data is not None:
					cells.append((key, data))
					cell_id.append(global_id)

				if data is None:
					key, num_nodes_per_cell, num_cells, global_id, first_index, last_index = _get_cell_info(f, line)
					if global_id != 0:
						cells.append((key, np.array([[0] * num_nodes_per_cell] * num_cells), first_index, last_index))
						cell_id.append(global_id)

			elif re.match("(|20|30)13", index):
				data, global_id, bounds_data, first_index, last_index = _read_faces(f, line)
				for key in data:
					facets.append((key, data[key], first_index, last_index))
					facet_id.append(global_id)
					bounds.append(bounds_data)

			elif index == "39":
				# logging.warning("Zone specification not supported yet. Skipping.")
				# _skip_close(f, line.count("(") - line.count(")"))
				t, n, i = _read_zones(f, line)
				if data is not None:
					zones[i] = (n, t)

			elif index == "45":
				# (45 (2 fluid solid)())
				obj = re.match("\\(45 \\([0-9]+ ([\\S]+) ([\\S]+)\\)\\(\\)\\)", line)
				if obj:
					warn(
						f"Zone specification not supported yet ({obj.group(1)}, {obj.group(2)}). "
						+ "Skipping.",
					)
				else:
					warn("Zone specification not supported yet.")

			else:
				warn(f"Unknown index {index}. Skipping.")
				# Skipping ahead to the next line with two closing brackets.
				_skip_close(f, line.count("(") - line.count(")"))

	points = np.concatenate(points)

	for thread in range(0, len(facets)):
		for face in range(0, len(facets[thread][1])):
			cell_1 = bounds[thread][face][0]
			cell_2 = bounds[thread][face][1]
			facet = facets[thread][1][face]
			cells = _add_facet_nodes_to_cell(facet, cells, cell_1, cell_2)

	cell_data = _assign_id_to_element(cell_data, cells, cell_id, "subdomains")
	facet_data = _assign_id_to_element(facet_data, facets, facet_id, "boundaries")

	# Gauge the cells with the first point_index.
	for k, c in enumerate(cells):
		cells[k] = (c[0], c[1] - first_point_index_overall)
		if cells[k][0] in ("hexahedron"):
			for i, cell in enumerate(cells[k][1]):
				cells[k][1][i] = _fix_hexa_orientation(points, cell.copy())

	# Gauge the facets with the first point_index.
	for k, f in enumerate(facets):
		facets[k] = (f[0], f[1] - first_point_index_overall)

	bounds_arr = []
	for bound in bounds:
		bounds_arr += bound.tolist()
	bounds_arr = np.asarray(bounds_arr)

	cell_data = {tags: [cell_data['subdomains'].reshape(-1), facet_data['boundaries'].reshape(-1)]}
	cells = _elements_adjust(cells)
	facets = _elements_adjust(facets, tags=bounds_arr)

	if return_zones:
		return Mesh(points, [cells, facets], point_data={}, cell_data=cell_data, field_data={}), zones
	else:
		return Mesh(points, [cells, facets], point_data={}, cell_data=cell_data, field_data={})


def write(filename, mesh, binary=False, tags='meshtags', zones=False):
	with open_file(filename, "wb") as fh:
		sec_count = 0

		# header
		fh.write(f'(0 "Created by: meshio {__version__} with ansys2meshio support")\n'.encode())

		# dimension
		num_points, dim = mesh.points.shape
		if dim not in [2, 3]:
			raise WriteError(f"Can only write dimension 2, 3, got {dim}.")
		fh.write((f"(2 {dim})\n").encode())

		# node section
		fh.write(f'(0 "Node Section")\n'.encode())

		# total number of nodes
		first_node_index = 1
		fh.write((f"(10 (0 {first_node_index:x} {num_points:x} {sec_count} {dim:x}))\n").encode())
		sec_count += 1

		# Write nodes
		key = "3010" if binary else "10"
		fh.write(
			f"({key} (1 {first_node_index:x} {num_points:x} 1 {dim:x})(\n".encode()
		)
		if binary:
			mesh.points.tofile(fh)
			fh.write(b"\n)")
			fh.write(b"End of Binary Section 3010)\n")
		else:
			np.savetxt(fh, mesh.points)#, fmt="%.16e")
			fh.write(b"))\n")

		meshio_to_ansys_type = {
			# "mixed": 0,
			"triangle": 1,
			"tetra": 2,
			"quad": 3,
			"hexahedron": 4,
			"pyramid": 5,
			"wedge": 6,
			# "polyhedral": 7,
		}

		ztypes = []

		# total number of cells
		# (2012 (zone-id first-index last-index type element-type))
		total_num_cells = 0
		if dim == 2:
			for key in ('triangle', 'quad'):
				total_num_cells += len(mesh.get_cells_type(key))
		elif dim == 3:
			for key in ('tetra', 'hexahedron', 'pyramid', 'wedge'):
				total_num_cells += len(mesh.get_cells_type(key))
		fh.write((f"(12 (0 1 {total_num_cells:x} 0 0))\n").encode())

		binary_dtypes = {
			# np.int16 is not allowed
			np.dtype("int32"): "2012",
			np.dtype("int64"): "3012",
		}

		# Write cells
		first_index = 1
		for cell_block in mesh.cells:
			cell_type = cell_block.type
			try:
				ansys_cell_type = meshio_to_ansys_type[cell_type]
			except KeyError:
				legal_keys = ", ".join(meshio_to_ansys_type.keys())
				raise KeyError(
					f"Illegal ANSYS cell type '{cell_type}'. (legal: {legal_keys})"
				)
			if (dim == 2 and cell_type in ('triangle', 'quad')) or (dim == 3 and cell_type in ('tetra', 'hexahedron', 'pyramid', 'wedge')):
				ztypes.append('fluid')
				celltags = mesh.get_cell_data(tags, cell_type)
				for tag in set(celltags):
					fh.write(f'(0 "Cells of zone {tag}")\n'.encode())
					values = cell_block.data[celltags == tag]
					key = binary_dtypes[values.dtype] if binary else "12"
					last_index = first_index + len(values) - 1
					fh.write(f"({key} ({tag:x} {first_index:x} {last_index:x} {sec_count} {ansys_cell_type}))\n".encode())
					# fh.write(
					# 	f"({key} ({tag:x} {first_index:x} {last_index:x} {sec_count} {ansys_cell_type})(\n".encode()
					# )
					# if binary:
					# 	(values + first_node_index).tofile(fh)
					# 	fh.write(b"\n)")
					# 	fh.write((f"End of Binary Section {key})\n").encode())
					# else:
					# 	np.savetxt(fh, values + first_node_index, fmt="%x")
					# 	fh.write(b"))\n")
					first_index = last_index + 1
					sec_count += 1

		meshio_to_ansys_type = {
			# "mixed": 0,
			"line": 2,
			"triangle": 3,
			"quad": 4
		}

		# total number of facets
		# (13 (zone-id first-index last-index type element-type))
		total_num_facets = 0
		if dim == 2:
			total_num_facets += len(mesh.get_cells_type('line'))
		elif dim == 3:
			for key in ('triangle', 'quad'):
				total_num_facets += len(mesh.get_cells_type(key))
		fh.write((f"(13 (0 1 {total_num_facets:x} 0 0))\n").encode())

		# Write faces
		first_index = 1
		for cell_block in mesh.cells:
			cell_type = cell_block.type
			# try:
			# 	ansys_cell_type = meshio_to_ansys_type[cell_type]
			# except KeyError:
			# 	legal_keys = ", ".join(meshio_to_ansys_type.keys())
			# 	raise KeyError(
			# 		f"Illegal ANSYS cell type '{cell_type}'. (legal: {legal_keys})"
			# 	)
			if (dim == 2 and cell_type == 'line' ) or (dim == 3 and cell_type in ('triangle', 'quad')):
				celltags = mesh.get_cell_data(tags, cell_type)
				ansys_cell_type = meshio_to_ansys_type[cell_type]
				for tag in set(celltags):
					fh.write(f'(0 "Faces of zone {tag}")\n'.encode())
					values = cell_block.data[celltags == tag] + first_node_index
					bounds = cell_block.tags[celltags == tag]
					ztypes.append('wall' if all(bounds[:, 1] == 0) else 'interior')
					key = binary_dtypes[values.dtype] if binary else "13"
					last_index = first_index + len(values) - 1
					fh.write(
						f"({key} ({tag:x} {first_index:x} {last_index:x} {sec_count} {ansys_cell_type})(\n".encode()
					)
					if binary:
						np.hstack([values, bounds]).tofile(fh)
						fh.write(b"\n)")
						fh.write((f"End of Binary Section {key})\n").encode())
					else:
						np.savetxt(fh, np.hstack([values, bounds]), fmt="%x")
						fh.write(b"))\n")
					first_index = last_index + 1
					sec_count += 1

		# Write zones
		# (39 (zone-id type NAME)())
		fh.write(f'(0 "Zone Sections")\n'.encode())
		cdict = mesh.cell_data_dict[tags]
		zcount = 1
		znames = ['ZONE_{}'.format(z) for z in range(len(ztypes))]
		for key in cdict.keys():
			for tag in set(cdict[key]):
				zname = znames[zcount - 1] if not zones else zones[tag][0]
				ztype = ztypes[zcount - 1] if not zones else zones[tag][1]
				fh.write(f"(39 ({tag} {ztype} {zname})())\n".encode())
				zcount += 1

register_format("ansys", [".msh"], read, {"ansys": write})
