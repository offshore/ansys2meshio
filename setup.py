from setuptools import setup


setup(
    name='ansys2meshio',
    version='1.0.0',
    author='Carlos A. S. Ferreira',
    author_email='casfe@dtu.dk',
    description=('This package intends to support the zone specification when converting a mesh file from ANSYS format using meshio.'),
    long_description=open('ansys2meshio/README.md', 'r').read(),
    packages=['ansys2meshio'],
    install_requires=['meshio>=5.3.4'],
    license='BSD',
    url='https://gitlab.gbar.dtu.dk/casfe/ansys2meshio'
)