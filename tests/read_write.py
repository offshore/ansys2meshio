import ansys2meshio
import os


mesh, zones = ansys2meshio.read(os.path.join('meshes', 'ball.msh'), return_zones=True)
mesh.write(os.path.join('meshes', 'ball.xdmf'))
mesh.write(os.path.join('meshes', 'ball_2.msh'), zones=zones)

mesh, zones = ansys2meshio.read(os.path.join('meshes', 'box.msh'), return_zones=True)
mesh.write(os.path.join('meshes', 'box.xdmf'))
mesh.write(os.path.join('meshes', 'box_2.msh'), zones=zones)